-module(tcbench).
-export([start/2, init/2, thread/1]).

start(Total_Rounds, Step) ->
  spawn(?MODULE, init, [Total_Rounds, Step]).

init(Total_Rounds, Step) ->
  Start_Time = os:timestamp(),
  create_threads(0, 0, Total_Rounds, Step, Start_Time).

create_threads(Total_Rounds, _, Total_Rounds, _, _) ->
  init:stop();

create_threads(Round, Step, Total_Rounds, Step, Last_Time) ->
  wait_threads(Round, 0, Total_Rounds, Step, Last_Time);

create_threads(Round, Created, Total_Rounds, Step, Last_Time) ->
  spawn(?MODULE, thread, [self()]),
  create_threads(Round, Created+1, Total_Rounds, Step, Last_Time).

wait_threads(Round, Step, Total_Rounds, Step, Last_Time) ->
  Time = os:timestamp(),
  Diff = timer:now_diff(Time, Last_Time),
  io:format("~p~n", [Diff div 1000]),
  create_threads(Round+1, 0, Total_Rounds, Step, Time);

wait_threads(Round, Started, Total_Rounds, Step, Last_Time) ->
  receive
    started ->
      wait_threads(Round, Started+1, Total_Rounds, Step, Last_Time)
  end.

thread(Parent) ->
  Parent ! started,
  receive
    stop -> ok
  end.
