import kilim.*;

public class KilimBenchNB extends Task {
  public static void main(String[] args) throws Exception {
    boolean noargs = args.length == 0;
    int rounds = noargs ? 10 : Integer.valueOf(args[0]);
    int step = noargs ? 100 : Integer.valueOf(args[1]);
    int nSchedulers = noargs ? 1 : Integer.valueOf(args[2]);
    int nThreadsPerScheduler = noargs ? 1 : Integer.valueOf(args[3]);
    Scheduler[] schedulers = new Scheduler[nSchedulers];

    System.out.println(String.format(
      "rounds: %d, step: %d, nSchedulers: %d, nThreadsPerScheduler: %d",
      rounds, step, nSchedulers, nThreadsPerScheduler));

    mainmb = new Mailbox<Msg>(step, step);
    for (int i = 0; i < nSchedulers; i++)
      schedulers[i] = new Scheduler(nThreadsPerScheduler);

    for (int r = 0; r < rounds; r++) {
      long beginTime = System.currentTimeMillis();
      for (int i = 0; i < step; i++) {
        KilimBenchNB t = new KilimBenchNB(i);
        t.setScheduler(schedulers[i % nSchedulers]);
        t.start();
      }
      for (int i = 0; i < step; i++) {
        Msg m = mainmb.getb(30000);
        if (m == null) {
          System.err.println("TIME OUT (30s)");
          System.exit(1);
        }
      }
      System.out.println(System.currentTimeMillis() - beginTime);
    }

    for (int i = 0; i < nSchedulers; i++)
      schedulers[i].shutdown();
  }

  static Mailbox<Msg> mainmb;

  int n;
  KilimBenchNB(int i) { n = i; }

  public void execute() throws Pausable {
    Msg mymsg = new Msg(n);
    mainmb.put(mymsg);
  }

  private static class Msg {
    int from;
    Msg(int f) { f = from; }
  }
}

