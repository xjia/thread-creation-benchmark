KILIM=$(HOME)/kilim
ROUND=10
STEP=100
CORES=2

erlang:
	erlc +native tcbench.erl
	erl -sname tcbench +P 100000000 -eval "tcbench:start($(ROUND), $(STEP))" -noinput

nberlang:
	erlc +native tcbenchnb.erl
	erl -sname tcbenchnb +P 100000000 -eval "tcbenchnb:start($(ROUND), $(STEP))" -noinput

kilim:
	mkdir -p classes/
	javac -cp $(KILIM)/classes/ -d classes/ KilimBench.java
	java -cp $(KILIM)/classes/:$(KILIM)/libs/asm-all-2.2.3.jar kilim.tools.Weaver -d classes/ classes/
	java -XX:-UseSplitVerifier -cp classes/:$(KILIM)/classes/ KilimBench $(ROUND) $(STEP) 4 8

nbkilim:
	mkdir -p classes/
	javac -cp $(KILIM)/classes/ -d classes/ KilimBenchNB.java
	java -cp $(KILIM)/classes/:$(KILIM)/libs/asm-all-2.2.3.jar kilim.tools.Weaver -d classes/ classes/
	java -XX:-UseSplitVerifier -cp classes/:$(KILIM)/classes/ KilimBenchNB $(ROUND) $(STEP) 4 8

haskell:
	rm -f tcbench tcbench.hi tcbench.o
	ghc -O3 -threaded -rtsopts tcbench.hs
	./tcbench $(ROUND) $(STEP) +RTS -N$(CORES)

nbhaskell:
	rm -f tcbenchnb tcbenchnb.hi tcbenchnb.o
	ghc -O3 -threaded -rtsopts tcbenchnb.hs
	./tcbenchnb $(ROUND) $(STEP) +RTS -N$(CORES)

