import Control.Concurrent
import Control.Concurrent.Chan
import System.Environment
import System.Exit
import System.Time

main = getArgs >>= start

start [s1,s2] = do
  startTime <- getClockTime
  createThreads 0 [] (read s1 :: Int) (read s2 :: Int) startTime

createThreads rounds chans total step lastTime
  | rounds == total       = exitWith ExitSuccess
  | length chans == step  = waitThreads rounds (reverse chans) total step lastTime
  | otherwise             = do ch <- newChan
                               forkIO (thread ch)
                               createThreads rounds (ch:chans) total step lastTime

waitThreads rounds [] total step lastTime = do
  currentTime <- getClockTime
  putStrLn $ show $ diffToMillisec $ diffClockTimes currentTime lastTime
  createThreads (rounds + 1) [] total step currentTime

waitThreads rounds (ch:chans) total step lastTime =
  readChan ch >>= \_ -> waitThreads rounds chans total step lastTime

thread ch = writeChan ch 1

diffToMillisec diff = toInteger (seconds * 1000) + (p `div` 1000000000)
  where h = tdHour diff
        m = tdMin diff
        s = tdSec diff
        p = tdPicosec diff
        seconds = (h * 60 + m) * 60 + s
